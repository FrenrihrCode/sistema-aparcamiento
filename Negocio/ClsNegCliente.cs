﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;

namespace Negocio
{
    public class ClsNegCliente
    {
        ClsDAOCliente daoCliente = new ClsDAOCliente();

        public DataTable GetClientes()
        {
            return daoCliente.GetClientes();
        }

        public bool InsertCliente(string nomcli, string telcli, string tiptar, string estado)
        {
            return daoCliente.InsertCliente(nomcli, telcli, tiptar, estado);
        }

        public bool UpdateCliente(int codcli, string nomcli, string telcli, string tiptar, string estado)
        {
            return daoCliente.UpdateCliente(codcli, nomcli, telcli, tiptar, estado);
        }

        public bool IsHour(int codcli)
        {
            return daoCliente.PayPerHour(codcli);
        }
    }
}
