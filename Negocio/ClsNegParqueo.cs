﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Datos;

namespace Negocio
{
    public class ClsNegParqueo
    {
        ClsDAOParqueo parqueo = new ClsDAOParqueo();
        public DataTable GetRegistros()
        {
            return parqueo.GetRegistros();
        }

        public int CheckPlacaInParqueo(string placa)
        {
            return parqueo.CheckPlacaInParqueo(placa);
        }

        public bool RegistrarEntrada(string feccre, int codcli, string placa, string fecini)
        {
            return parqueo.RegistrarEntrada(feccre, codcli, placa, fecini);
        }
        public bool RegistrarSalida(int invnum, string fecfin, double monto, string time)
        {
            return parqueo.RegistrarSalida(invnum, fecfin, monto, time);
        }

        public double CalcularTiempo(int invnum, string fecfin)
        {
            return parqueo.CalcularTiempo(invnum, fecfin);
        }
    }
}
