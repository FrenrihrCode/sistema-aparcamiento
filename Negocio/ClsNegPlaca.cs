﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Datos;

namespace Negocio
{
    public class ClsNegPlaca
    {
        ClsDAOPlaca daoPlaca = new ClsDAOPlaca();

        public DataTable GetPlacas()
        {
            return daoPlaca.GetPlacas();
        }

        public bool InsertPlaca(int codcli, string placa, string caracteristicas, string estado)
        {
            return daoPlaca.InsertPlaca(codcli, placa, caracteristicas, estado);
        }

        public bool UpdateVehiculo(int codcli, string placa, string caracteristicas, string estado)
        {
            return daoPlaca.UpdateVehiculo(codcli, placa, caracteristicas, estado);
        }

        public int CheckPlaca(string placa)
        {
            return daoPlaca.CheckPlaca(placa);
        }
    }
}
