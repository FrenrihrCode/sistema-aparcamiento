﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;

namespace Presentacion
{
    public partial class Form1 : Form
    {
        DataTable dataCliente = new DataTable();
        DataTable dataVehiculo = new DataTable();
        DataTable dataRegistro = new DataTable();
        //
        ClsNegCliente negocioCliente = new ClsNegCliente();
        ClsNegPlaca negocioPlaca = new ClsNegPlaca();
        ClsNegParqueo negocioParqueo = new ClsNegParqueo();

        int pagoMensual = 300;
        double pagoDia = 1.2;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            timer1.Enabled = true;
            dataCliente = negocioCliente.GetClientes();
            dataVehiculo = negocioPlaca.GetPlacas();
            dataRegistro = negocioParqueo.GetRegistros();
            dgvRegistros.DataSource = dataRegistro;
            //dgvRegistros.Refresh();
            dgvRegistros.Update();
            dgvRegistros.Sort(dgvRegistros.Columns["invnum"], ListSortDirection.Descending);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblHora.Text = "HORA: " + DateTime.Now.ToString("hh:mm:ss");
            lblFecha.Text = "FECHA: " + DateTime.Now.ToLongDateString();
        }

        private void btnPlaca_Click(object sender, EventArgs e)
        {
            dataCliente = negocioCliente.GetClientes();
            FormPlaca formPlaca = new FormPlaca(dataCliente, negocioPlaca);
            formPlaca.Show();
        }

        private void btnCliente_Click(object sender, EventArgs e)
        {
            FormCliente formPlaca = new FormCliente(negocioCliente);
            formPlaca.Show();
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            string placa = txtPlacaParqueo.Text.Trim().ToUpper();
            string fecha = DateTime.Now.ToString();
            if (!string.IsNullOrEmpty(placa))
            {
                int codCliPlaca = negocioPlaca.CheckPlaca(placa);
                if (codCliPlaca != -1)
                {
                    int registroNumber = negocioParqueo.CheckPlacaInParqueo(placa);
                    if (registroNumber != -1)
                    {
                        bool isHour = negocioCliente.IsHour(codCliPlaca);
                        if (isHour)
                        {
                            double tiempo = negocioParqueo.CalcularTiempo(registroNumber, fecha);
                            double montoPagar = Math.Round(tiempo * pagoDia, 2);
                            string timeReg = TimeSpan.FromHours(tiempo).ToString();
                            negocioParqueo.RegistrarSalida(registroNumber, fecha, montoPagar, timeReg);
                        }
                        else
                        {
                            negocioParqueo.RegistrarSalida(registroNumber, fecha, pagoMensual, "");
                        }
                    }
                    else
                    {
                        bool res = negocioParqueo.RegistrarEntrada(fecha, codCliPlaca, placa, fecha);
                        if (res)
                        {
                            MessageBox.Show("Nuevo registro generado");
                        }
                    }
                }
            }
            dataRegistro = negocioParqueo.GetRegistros();
            dgvRegistros.DataSource = dataRegistro;
            dgvRegistros.Update();
            dgvRegistros.Sort(dgvRegistros.Columns["invnum"], ListSortDirection.Descending);
        }
    }
}
