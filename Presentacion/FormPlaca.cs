﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;

namespace Presentacion
{
    public partial class FormPlaca : Form
    {
        DataTable dt;
        ClsNegPlaca negocio;
        public FormPlaca(DataTable dt, ClsNegPlaca negocio)
        {
            this.dt = dt;
            this.negocio = negocio;
            InitializeComponent();
        }

        private void FormPlaca_Load(object sender, EventArgs e)
        {
            Console.WriteLine(dt.Rows);
            comboClientes.DisplayMember = "nomcli";
            comboClientes.ValueMember = "codcli";
            comboClientes.DataSource = dt;
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            int codcli = int.Parse(comboClientes.SelectedValue.ToString());
            string placa = txtPlaca.Text.Trim();
            string caracteristicas = txtCaracteristicas.Text.Trim();
            string estado = ckbEstado.Checked ? "A" : "I";
            if (!string.IsNullOrEmpty(placa))
            {
                
                bool res = negocio.InsertPlaca(codcli, placa, caracteristicas, estado);
                if (res)
                {
                    MessageBox.Show("Nueva Placa Ingresada");
                }
                else
                {
                    MessageBox.Show("Ocurrió un problema");
                }
            }
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            int codcli = int.Parse(comboClientes.SelectedValue.ToString());
            string placa = txtPlaca.Text.Trim();
            string caracteristicas = txtCaracteristicas.Text.Trim();
            string estado = ckbEstado.Checked ? "A" : "I";

            if (!string.IsNullOrEmpty(placa))
            {
                var confirmResult = MessageBox.Show("Estás seguro de querer actualizar la placa " + placa + "?",
                                         "Actualizar?",
                                         MessageBoxButtons.YesNo);
                if (confirmResult == DialogResult.Yes)
                {
                    bool res = negocio.UpdateVehiculo(codcli, placa, caracteristicas, estado);
                    if (res)
                    {
                        MessageBox.Show("Placa Actualizada");
                    }
                    else
                    {
                        MessageBox.Show("Ocurrió un problema");
                    }
                }
            }
        }
    }
}
