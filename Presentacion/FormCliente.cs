﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;

namespace Presentacion
{
    public partial class FormCliente : Form
    {
        ClsNegCliente negocio;
        public FormCliente(ClsNegCliente negocio)
        {
            this.negocio = negocio;
            InitializeComponent();
        }

        private void Clientes_Load(object sender, EventArgs e)
        {
            radHoras.Checked = true;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            txtCodigo.Enabled = true;
            txtNombre.Enabled = true;
            txtTelefono.Enabled = true;
            ckbEstado.Enabled = true;
            btnGrabar.Enabled = true;
            btnActualizar.Enabled = true;
        }

        private void btnGrabar_Click(object sender, EventArgs e)
        {
            string nombre = txtNombre.Text.Trim();
            string telefono = txtTelefono.Text.Trim();
            string estado = ckbEstado.Checked ? "A" : "X";
            string tipoTarifa = radHoras.Checked ? "H" : "M";

            if (!string.IsNullOrEmpty(nombre))
            {

                bool res = negocio.InsertCliente(nombre, telefono, tipoTarifa, estado);
                if (res)
                {
                    MessageBox.Show("Nuevo Cliente Ingresado");
                }
                else
                {
                    MessageBox.Show("Ocurrió un problema");
                }
            }
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            string codigo = txtCodigo.Text.Trim();
            string nombre = txtNombre.Text.Trim();
            string telefono = txtTelefono.Text.Trim();
            string estado = ckbEstado.Checked ? "A" : "X";
            string tipoTarifa = radHoras.Checked ? "H" : "M";
            if (!string.IsNullOrEmpty(nombre) && !string.IsNullOrEmpty(codigo))
            {
                if (int.TryParse(codigo, out int c))
                {
                    var confirmResult = MessageBox.Show("Estás seguro de querer actualizar el registro " + codigo + "?",
                                         "Actualizar?",
                                         MessageBoxButtons.YesNo);
                    if (confirmResult == DialogResult.Yes)
                    {
                        int codcli = int.Parse(codigo);
                        bool res = negocio.UpdateCliente(codcli, nombre, telefono, tipoTarifa, estado);
                        if (res)
                        {
                            MessageBox.Show("Cliente Actualizado");
                        }
                        else
                        {
                            MessageBox.Show("Ocurrió un problema");
                        }
                    }
                }
            }
        }
    }
}
