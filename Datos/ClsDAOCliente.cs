﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class ClsDAOCliente : ClsDAO
    {
        public DataSet ds = new DataSet();
        public DataTable tableCliente = new DataTable();

        public DataTable GetClientes()
        {
            try
            {
                string sql = "SELECT * FROM clientes";
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = cmd;
                adapter.Fill(ds, "Cliente");
                tableCliente = ds.Tables["Cliente"];
                return tableCliente;
            }
            catch (Exception err)
            {
                Console.Write(err.ToString());
                return null;
            }
        }

        public bool InsertCliente(string nomcli, string telcli, string tiptar, string estado)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("InsertCliente", conn);
                cmd.Parameters.Add("@NomCli", SqlDbType.VarChar, 60, "nomcli");
                cmd.Parameters.Add("@TelCli", SqlDbType.VarChar, 50, "telcli");
                cmd.Parameters.Add("@TipTar", SqlDbType.VarChar, 1, "tiptar");
                cmd.Parameters.Add("@Estado", SqlDbType.VarChar, 1, "estado");
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.InsertCommand = cmd;
                adapter.InsertCommand.CommandType = CommandType.StoredProcedure;
                DataRow fila = tableCliente.NewRow();
                fila["nomcli"] = nomcli;
                fila["telcli"] = telcli;
                fila["tiptar"] = tiptar;
                fila["estado"] = estado;
                tableCliente.Rows.Add(fila);
                adapter.Update(tableCliente);
                return true;
            }
            catch (Exception err)
            {
                Console.Write(err.ToString());
                return false;
            }
        }

        public bool UpdateCliente(int codcli, string nomcli, string telcli, string tiptar, string estado)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("UpdateCliente", conn);
                cmd.Parameters.Add("@CodCli", SqlDbType.Int).SourceColumn = "codcli";
                cmd.Parameters.Add("@NomCli", SqlDbType.VarChar, 60, "nomcli");
                cmd.Parameters.Add("@TelCli", SqlDbType.VarChar, 50, "telcli");
                cmd.Parameters.Add("@TipTar", SqlDbType.VarChar, 1, "tiptar");
                cmd.Parameters.Add("@Estado", SqlDbType.VarChar, 1, "estado");

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.UpdateCommand = cmd;
                adapter.UpdateCommand.CommandType = CommandType.StoredProcedure;
                DataRow[] fila = tableCliente.Select(String.Format("codcli = '{0}'", codcli));
                fila[0]["nomcli"] = nomcli;
                fila[0]["telcli"] = telcli;
                fila[0]["tiptar"] = tiptar;
                fila[0]["estado"] = estado;
                adapter.Update(tableCliente);
                return true;
            }
            catch (Exception err)
            {
                Console.Write(err.ToString());
                return false;
            }
        }

        public bool PayPerHour(int codcli)
        {
            try
            {
                DataRow[] fila = tableCliente.Select(String.Format("codcli = '{0}'", codcli));
                if (fila.Length != 0)
                {
                    Console.Write(fila[0]["tiptar"]);
                    string tiptar = fila[0]["tiptar"].ToString();
                    return tiptar == "H";
                }
                return false;
            }
            catch (Exception err)
            {
                Console.Write(err.ToString());
                return false;
            }
        }
    }
}
