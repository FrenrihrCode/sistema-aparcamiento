﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Datos
{
    public class ClsDAOPlaca : ClsDAO
    {
        public DataSet ds = new DataSet();
        public DataTable tablePlaca = new DataTable();

        public DataTable GetPlacas()
        {
            try
            {
                string sql = "SELECT * FROM vehiculos";
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = cmd;
                adapter.Fill(ds, "Placa");
                tablePlaca = ds.Tables["Placa"];
                return tablePlaca;
            }
            catch (Exception err)
            {
                Console.Write(err.ToString());
                return null;
            }
        }

        public bool InsertPlaca(int codcli, string placa, string caracteristicas, string estado)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("InsertPlaca", conn);
                cmd.Parameters.Add("@CodCli", SqlDbType.Int).SourceColumn = "codcli";
                cmd.Parameters.Add("@Placa", SqlDbType.VarChar, 10, "placa");
                cmd.Parameters.Add("@Caracteristicas", SqlDbType.VarChar, 50, "caracteristicas");
                cmd.Parameters.Add("@Estado", SqlDbType.VarChar, 1, "estado");
                Console.WriteLine(ds.Tables.ToString());
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.InsertCommand = cmd;
                adapter.InsertCommand.CommandType = CommandType.StoredProcedure;
                DataRow fila = tablePlaca.NewRow();
                fila["codcli"] = codcli;
                fila["placa"] = placa;
                fila["caracteristicas"] = caracteristicas;
                fila["estado"] = estado;
                tablePlaca.Rows.Add(fila);
                adapter.Update(tablePlaca);
                return true;
            }
            catch (Exception err)
            {
                Console.Write(err.ToString());
                return false;
            }
        }

        public bool UpdateVehiculo(int codcli, string placa, string caracteristicas, string estado)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("UpdateVehiculo", conn);
                cmd.Parameters.Add("@CodCli", SqlDbType.Int).SourceColumn = "codcli";
                cmd.Parameters.Add("@Placa", SqlDbType.VarChar, 10, "placa");
                cmd.Parameters.Add("@Caracteristicas", SqlDbType.VarChar, 50, "caracteristicas");
                cmd.Parameters.Add("@Estado", SqlDbType.VarChar, 1, "estado");

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.UpdateCommand = cmd;
                adapter.UpdateCommand.CommandType = CommandType.StoredProcedure;
                DataRow[] fila = tablePlaca.Select(String.Format("placa = '{0}'", placa));
                fila[0]["codcli"] = codcli;
                fila[0]["caracteristicas"] = caracteristicas;
                fila[0]["estado"] = estado;
                adapter.Update(tablePlaca);
                return true;
            }
            catch (Exception err)
            {
                Console.Write(err.ToString());
                return false;
            }
        }

        public int CheckPlaca(string placa)
        {
            try
            {
                //string filterStatement = String.Format("placa = '{0}'", placa);
                DataRow[] fila = tablePlaca.Select(String.Format("placa = '{0}'", placa));
                if (fila.Length != 0)
                {
                    Console.Write(fila[0]["codcli"]);
                    return (int)fila[0]["codcli"];
                }
                return -1;
            }
            catch (Exception err)
            {
                Console.Write(err.ToString());
                return -1;
            }
        }
    }
}
