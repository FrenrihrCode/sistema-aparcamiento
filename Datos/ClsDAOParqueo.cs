﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Datos
{
    public class ClsDAOParqueo : ClsDAO
    {
        //public DataSet ds = new DataSet();
        public DataSet ds;
        public DataTable tableRegistro = new DataTable();

        public DataTable GetRegistros()
        {
            try
            {
                string sql = "SELECT * FROM registro";
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataAdapter adapter = new SqlDataAdapter();
                ds = new DataSet();
                adapter.SelectCommand = cmd;
                adapter.Fill(ds, "Registro");
                tableRegistro = ds.Tables["Registro"];
                return tableRegistro;
            }
            catch (Exception err)
            {
                Console.Write(err.ToString());
                return null;
            }
        }

        public int CheckPlacaInParqueo(string placa)
        {
            try
            {
                string exp = String.Format("placa = '{0}' AND fecfin IS NULL", placa);
                string sortOrder = "invnum DESC";
                DataRow[] fila = tableRegistro.Select(exp, sortOrder);
                if (fila.Length != 0)
                {
                    Console.Write(fila[0]["invnum"]);
                    if (int.TryParse(fila[0]["invnum"].ToString(), out int c))
                    {
                        int invnum = int.Parse(fila[0]["invnum"].ToString());
                        return invnum;
                    }
                }
                return -1;
            }
            catch (Exception err)
            {
                Console.Write(err.ToString());
                return -1;
            }
        }


        public bool RegistrarEntrada(string feccre, int codcli, string placa, string fecini)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("AbrirRegistro", conn);
                cmd.Parameters.Add("@FecCre", SqlDbType.Date).SourceColumn = "feccre";
                cmd.Parameters.Add("@CodCli", SqlDbType.Int).SourceColumn = "codcli";
                cmd.Parameters.Add("@Placa", SqlDbType.VarChar, 10, "placa");
                cmd.Parameters.Add("@FecIni", SqlDbType.Date).SourceColumn = "fecini";
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.InsertCommand = cmd;
                adapter.InsertCommand.CommandType = CommandType.StoredProcedure;
                DataRow fila = tableRegistro.NewRow();
                fila["feccre"] = feccre;
                fila["codcli"] = codcli;
                fila["placa"] = placa;
                fila["fecini"] = fecini;
                tableRegistro.Rows.Add(fila);
                adapter.Update(tableRegistro);
                return true;
            }
            catch (Exception err)
            {
                Console.Write(err.ToString());
                return false;
            }
        }

        public bool RegistrarSalida(int invnum, string fecfin, double monto, string time)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("CerrarRegistro", conn);
                cmd.Parameters.Add("@InvNum", SqlDbType.Int).SourceColumn = "invnum";
                cmd.Parameters.Add("@FecFin", SqlDbType.Date).SourceColumn = "fecfin";
                cmd.Parameters.Add("@Monto", SqlDbType.Decimal).SourceColumn = "monto";
                cmd.Parameters.Add("@Time", SqlDbType.Time).SourceColumn = "time";

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.UpdateCommand = cmd;
                adapter.UpdateCommand.CommandType = CommandType.StoredProcedure;
                DataRow[] fila = tableRegistro.Select(String.Format("invnum = '{0}'", invnum));
                fila[0]["fecfin"] = fecfin;
                fila[0]["monto"] = monto;
                if (time == "")
                {
                    fila[0]["time"] = DBNull.Value;
                }
                else
                {
                    fila[0]["time"] = time;
                }
                adapter.Update(tableRegistro);
                    
                return true;
            }
            catch (Exception err)
            {
                Console.Write(err.ToString());
                return false;
            }
        }

        public double CalcularTiempo(int invnum, string fecfin)
        {
            try
            {
                DataRow[] fila = tableRegistro.Select(String.Format("invnum = '{0}'", invnum));
                if (fila.Length != 0)
                {
                    Console.Write(fila[0]["fecini"]);
                    string fecini = fila[0]["fecini"].ToString();
                    DateTime iniDate = Convert.ToDateTime(fecini);
                    DateTime finDate = Convert.ToDateTime(fecfin);
                    Console.Write(finDate);
                    TimeSpan ts = finDate - iniDate;
                    Console.Write(ts.TotalHours);
                    return ts.TotalHours;
                }
                DataRow[] filaDis = tableRegistro.Select(String.Format("NewRegistro = '{0}'", invnum));
                if (filaDis.Length != 0)
                {
                    Console.Write(filaDis[0]["fecini"]);
                    string fecini = filaDis[0]["fecini"].ToString();
                    DateTime iniDate = Convert.ToDateTime(fecini);
                    DateTime finDate = Convert.ToDateTime(fecfin);
                    Console.Write(finDate);
                    TimeSpan ts = finDate - iniDate;
                    Console.Write(ts.TotalHours);
                    return ts.TotalHours;
                }
                return -1;
            }
            catch (Exception err)
            {
                Console.Write(err.ToString());
                return -1;
            }
        }
    }
}
